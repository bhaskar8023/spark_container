from setuptools import setup, find_packages
import os
import codecs

here=os.path.abspath(os.path.dirname(__file__))
with codecs.open(os.path.join(here,"README.md"), encoding="utf-8") as fh:
    long_description=fh.read()

setup(
    name='PySpark MySql Database Load',
    version='v1.0',
    author='Bhaskar Kandala',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    install_requires=[
        "pyspark>3.0.0",
        "pandas>=1.2.4"
    ],
    python_requires='>=3.9'

)