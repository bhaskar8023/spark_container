This Repo is used to containerize a simple PySpark application using Docker, push the created image to GitLab Container registers and python code to GitLab Package registry, which acts as a PyPi registry.

In order to achive functionality, the below tech stack is required.

1) Docker instalation
2) python v3
3) GitLab account
4) VS Code
5) Basic knowledge of MySql, Pyspark and docker, Unix commands 

## Step 1: Create python virtual enviroment
Create a python virtual environment to isolate the developement work space and install the necessary packges that are required for the PySpark application in the virtual enviroment.

Commands to create virtual env
   
    cd <cd to path where project is to be created>
   
    python -m venv .  # create venv in the project root directory
  
    source venv/bin/activate #activate the virtual environemnt

## Step 2: Install packages in the project virtual environment using requirements.ini file

Create a `requirements.ini` file in the project directory with the list of packages to install; for this project, I am installing the below packages. This file will be used later to create the final application image using Docker file.

* format: <package_name>[>/==/>=/<=]<release_version>
     * pyspark>3.0.0
     * pandas>=1.2.4 

## Step 3: Add MySQL JDBC JAR file
As we are loading data to MySql Database, a JDBC driver is required for the spark to communicate with MySql server that is running on 3306. You can download any version of the mysql JDBC driver; for example, `http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.23.tar.gz`. After downloading the driver, add the extracted jar file to `*site-packages/pyspark/jars/` of your project, where spark checks for 3rd party drivers.

## step 4: Application code
Now, write a simple Pysaprk application that reads a file from the specified directory and appends data to Mysql Table using the mysql connector. Then, the application prints the total number of records loaded on a single day and total number of records in the table. See `db_load/db_load.py` file in the repository. url,password,user are passed at runtime as they are sensitive infomration.

Note: The current application runs in local mode because `local[*]` is embedded in the code.

## Step 5: Create Application Docker Image using Dockerfile
Steps 1 to 4 are aggregated as commands in the `Dockerfile`. The `Dockerfile` in this repository has a list of commands that uses `python:3.7.4-buster` as the base image in which we create the following:
  * Python virtual environemnt
  * Install spark and additional packages using `requirements.txt`
  * Add MySql connector
  * Create a optimized image from as image 
  * Install Java for spark run time
  * Copy the application code to image
  * Run application code using `CMD` command

The final step to create the application image is as follows. Run the below command that uses the Dockerfile in the current directory and creates an image.

        docker build -t <name_of_the_image> . 

        Example: docker build -t pyspark_db_load .

## Step 6: Test application image locally using docker-compose.yaml file
A `docker-compose.yaml` calls the docker images and orchestrates the execution of the images/applications, ingests secrets(passwords, usernames etc.), attaches volumes, passes environment variables, sets dependecies between applications, exposes ports, etc.

The details of tags in the `docker-compose.yaml` file are as follows.
  *  `services`: Calls the list of applications/images to be run. There are two services - `mysqldb`(MySQL Database server) and `pyspark` (application image created in step 5).
  * `image`: This tag is used to call the image of choice. 
      * `image: mysql`: Pulls the latest mysql image from dockerhub as there is no tag mentioned after mysql
      * `image: pyspark_db_load`: Calls the main application image created in step 5

Thus, a `docker-compose.yaml` has been created. However, when the command `docker compose up` is executed, you will see a failure. This is because the below variables are yet to be configured.
  * `mysqldb` service needs to be configured with the following enviorment variables when the service starts
  
      * MYSQL_ROOT_USER= "<username>"
      * MYSQL_ROOT_PASSWORD= "<password>"
      * MYSQL_ROOT_HOST="<host deault '%'>"

The above variables hold sensitive information; hence, it is not directly embed them in the docker-compose.yaml file instead we will 
create a file which holds this information (here it is .mysql_env.txt and we will not checkin this to GitLab project repository) and inject it into the mysqldb service using `env_file` tag(one way of passing environment variables)

  * `ports`: we need to expose the Mysql Server running on port 3306 to outside world so that pyspark application will be able reach mysql server. You can map this this to any port but make a note of the port you are mapping to as will need this port number to be mentioned in the jdbc url string which we will pass as environment variable to `pyspark` service.

  * `volumes`: Without allocating space our application does not make sense at end of the you have to store and read the data from a physical location.
    By default `mysql` stores all of it information in `/var/lib/mysql` path so we are going to bind this path to our local system path. If in case you terminate/delete mysql service you are not going to loss the data as its stored on the machine it is running on. So, when you restart the container it will lookup the path under the volumes tag and restores the data back.

    Our PySpark application reads and loads the data file `daily_file` that is mounted on to `/home` directory inside the `pyspark_db_load` image. `/home` directory inside the container is created if it does not exists when the application container starts. Just as an example my data file on my machine resides in `/Users/pyspark_app` directory which is what i am going to bind in the `pyspark` service volumnes tag.

we also need to configure the environemnt variables for `pyspark`, inside the `db_load/db_load.py` we have used the `os` module to pass the drive url, user and password at run time. So, we will follow the same approch the we used for `mysqldb` service.

  * `Dependency`: ake sure that `pyspark` service runs after the `mysqldb` is up and running to set this dependency we use `depends_on` tag. If the depedency is not set `pyspark` service will not be able to reach mysql server as `mysqldb` service might still be in startup phase, so we first want the database up and the start loading the data.

  * `Database and Table`: Lastly we need to create a database and table inside the database with 4 columns. Do this before running `docker compose up` by running the following commands:
   
        docker run -it --rm --name <name> -p <any_open_port>:3306 --mount "type=bind,src=<host_path_dir>,dst=/var/lib/mysql" -e MYSQL_ROOT_PASSWORD=<paswrd> -e MYSQL_ROOT_USER=<user_name> -d mysql

The above command returns a container_id, use the container_id to login to mysql command line

        docker exec -it <Container_id> /bin/sh

        mysql -u <user_name> -p

        create database sandbox;

        create table sandbox.EVENT_PRICE_TBL
        (EVENTID VARCHAR(20), EVENTNAME VARCHAR(20), EVENTCOST VARCHAR(20), LOAD_DT DATE);

exit and stop the mysql container using the container_id
       
       docker stop <container_id>

sample rows in `daily_file`

       600,State Fare,20
       800,NBA Game,40

Now, we are all set to run the using `docker-compose.yaml`, inside the project directory run the below command.
      
         docker-compose up
      
we see containers starting to run and prints the logs on terminal. After few minutes you can log in to mysql container using the container_id and query the data in the table.

     Reading Data from Daily File......                                              
     pyspark_1  | -------------------------------------------------------------
     pyspark_1  | Total records appended to EVENT_PRICE_TBL Table: 3
     pyspark_1  | Total records in EVENT_PRICE_TBL Table: 6
     pyspark_1  | -------------------------------------------------------------

Output from the table.

     mysql> select * from EVENT_PRICE_TBL;
     +---------+-----------+-----------+------------+
     | EVENTID | EVENTNAME | EVENTCOST | LOAD_DT    |
     +---------+-----------+-----------+------------+
     | 900     | Astar     | 50        | 2022-02-08 |
     | 600     | zzz       | 20        | 2022-02-08 |
     | 800     | xx        | 40        | 2022-02-08 |
     | 600     | state Fare| 20        | 2022-02-20 |
     | 900     | Astar     | 50        | 2022-02-20 |
     | 800     | NBA Game  | 40        | 2022-02-20 |
     +---------+-----------+-----------+------------+
     6 rows in set (0.01 sec)

Yay!!!. We have our data loaded next we need to terminate the mysql service if you would like to. Use the below commands on the terminal where you executed docker compose up
      
      Ctrl + D
      
      docker compose down

This concludes our testing. Next we are going to push the code to GitLab project repository and see how we can leverage CI (contunious Intigration).

## Step 7: Contunious Intigration Pipeline using GitLab
Let's see how we can psuh the code to GitLab and also see how to push images to GitLab Container registry and packege registry using `.gitlab-ci.yml` file. There are many things one needs to understand about the various tags in .gitlab-ci.yml` file please reffere https://docs.gitlab.com/ee/ci/yaml/index.html
 
Container regitry: we will push the application image created in `step 5` to container image, later on we can pull the image from this repository and deploy it else where. See `dockerregistery_upload` tag in `.gitlab-ci.yml` file

Package registy: This is similar to PyPi Registry all the python code goes in here. see `pypiregister_upload` tag in `.gitlab-ci.yml` file

Configure Deploy tokens in yout GitLab account follow the instuctions in the offical GitLab Documentation https://docs.gitlab.com/ee/user/project/deploy_tokens/ on how to setup. In the `.gitlab-ci.yml` file you will see `TOKEN_PASSWORD` and `TOKEN_USERNAME` this is where the configured Deploy tokens will come in to picture and will grant us with permissions to push code to `container` and `package` registry.

When you push code or any modifications GitLab automatically triggers a pipeline by excuting `.gitlab-ci.yml` in the gitlab runner. All the stages in the .gitlab-ci.yml file are excuted and if there is any error the pipeline fails which is some thing we need to invistigate and fix. If all goes through you should be able to see the images in `container` and `package` registry.

## Comments
In the next project we will see how we can leverage images and deploy them on Azure kubernetes service using `Terrafrorm` and `Helm charts`.







    
    
    
 
