FROM python:3.7.4-buster as builder

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN --mount=type=secret,id=git_secrets export $(cat /run/secrets/git_secrets)

COPY requirements.txt .
RUN pip install -q pip --upgrade && pip install -r requirements.txt

#add mysql jar path to python venv
RUN wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.23.tar.gz -O - | tar -xz -C /opt/venv/lib/python3.7/site-packages/pyspark/jars/ \
    && mv -f /opt/venv/lib/python3.7/site-packages/pyspark/jars/mysql-connector-java-8.0.23/*jar /opt/venv/lib/python3.7/site-packages/pyspark/jars/ \
    && rm -Rf /opt/venv/lib/python3.7/site-packages/pyspark/jars/mysql-connector-java-8.0.23

#Add JDK

FROM python:3.7.4-slim-buster

RUN echo "deb http://security.debian.org/debian-security stretch/updates main" >> /etc/apt/sources.list                                                   
RUN mkdir -p /usr/share/man/man1 && \
    apt-get update -y && \
    apt-get install -y openjdk-8-jdk    

COPY --from=builder /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"


COPY db_load/ db_load/

#Run Pyspark script
CMD python -m db_load.db_load