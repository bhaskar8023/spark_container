#!/usr/bin/env python3
#from pkg import dependent
import os
from pyspark import sql
from pyspark.sql import SparkSession
from pyspark.sql.context import SQLContext
from pyspark.sql.functions import *
from pyspark.sql.types import *

#testing the dependency --ignore
#print (dependent.sum([1,2,3,4])) 

#Here we will spark in stand alone mode
spark=SparkSession.builder.master("local[*]").appName("Mysql Database load").getOrCreate()
spark.sparkContext.setLogLevel("ERROR")
sqlContext=SQLContext(spark)


#file Schema
schema=StructType([StructField('EVENTID',StringType(),nullable=True),
                    StructField('EVENTNAME',StringType(),nullable=True),
                    StructField('EVENTCOST',StringType(),nullable=True)]) 

print("Reading Data from Daily File......")

#Creating dataframe with file data
daily_file_rdd=spark.sparkContext.textFile("/home/daily_file").map(lambda x: x.split(",")).toDF(schema)

#Add Load Time to the records
tgt_df=daily_file_rdd.withColumn("LOAD_DT",current_date())

#append data to MySql Table 
tgt_df.write.mode("append").format('jdbc').options(url=os.getenv("url"),
                                            dbtable="EVENT_PRICE_TBL",
                                           user=os.getenv('user'),
                                            password=os.getenv('password')
                                            ).save()

df=spark.read.format("jdbc").options(url=os.getenv("url"),
                                      dbtable="EVENT_PRICE_TBL",
                                     user=os.getenv('user'),
                                     password=os.getenv('password')).load()

#df.createOrReplaceTempView("Loaded_Table")
#rec_count=sqlContext.sql("Select count(*) from Loaded_Table")

#print Counts
print("-------------------------------------------------------------")
print("Total records appended to EVENT_PRICE_TBL Table: {}".format(str(tgt_df.count())))
print("Total records in EVENT_PRICE_TBL Table: {}".format(str(df.count())))
print("-------------------------------------------------------------")